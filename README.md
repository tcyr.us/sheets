# Sheet Music Site for Jekyll

This is a port of my [Grav Theme](https://gitlab.com/tcyr.us/grav-theme-sheet-music) and
my [Grav Plugin](https://gitlab.com/tcyr.us/grav-plugin-synthesia-data) into one
unified codebase.

Some functionality had to be removed due to the differences between Grav and Jekyll:

- Pagination
- Breadcrubs

This version also adds a JavaScript fallback for the Synthesia Plugin since
running Jekyll in Safe Mode would disable `_plugins/synthesia.rb`.

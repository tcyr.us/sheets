require 'nokogiri'

Jekyll::Hooks.register :site, :pre_render do |site|
  site.collections['sheets'].docs.each do |page|
    if (!(page.data['synthesia'].nil?))
      synthesia_dir = page.site.source + '/files/'
      synthesia_fn = page.data['synthesia']
      doc = File.open(synthesia_dir + synthesia_fn) { |f| Nokogiri::XML(f) }
      songs = doc.xpath('/SynthesiaMetadata/Songs/Song')
      page.data['synthesia_data'] = songs.map do |song|
        {
          'unique_id' => song['UniqueId'],
          'title' => song['Title'],
          'subtitle' => song['Subtitle'],
          'arranger' => song['Arranger'],
          'composer' => song['Composer'],
          'copyright' => song['Copyright'],
          'famous' => song['MadeFamousBy']
        }
      end
    end
  end
end

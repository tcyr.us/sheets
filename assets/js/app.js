$(function() {
    $(document).foundation();

    var $pdfOrbit = $('#pdf-orbit');
    if ($pdfOrbit.length !== 0) {
        new Foundation.PdfOrbit($pdfOrbit);
    }

    // Fallback if Synthesia Plugin is Disabled
    var listGroupDetail = function(el, key, val) {
        if (typeof val === 'undefined') return;
        el.prepend(
            '<div class="grid-x grid-padding-y"><div class="cell shrink">' +
            key +
            '</div><div class="cell auto text-right">' +
            val +
            '</div></div>'
        );
    };

    var $synthInfo = $('#synthesia-info');
    var $sheetInfo = $('#sheet-info')
    if (!$.trim($synthInfo.html())) {
        // Find Synthesia URL
        var synthesiaUrl = $('a[href$=".synthesia"]').attr('href');

        if (typeof synthesiaUrl !== "undefined") {
            $.ajax({
                type: "GET",
                url: synthesiaUrl,
                dataType: "xml",

                error: function(e) {
                    console.error("XML reading Failed: ", e);
                },

                success: function(resp) {
                    $(resp).find("SynthesiaMetadata > Songs > Song").each(function() {
                        listGroupDetail($synthInfo, 'Made Famous By', $(this).attr('MadeFamousBy'));
                        listGroupDetail($synthInfo, 'Copyright', $(this).attr('Copyright'));
                        listGroupDetail($synthInfo, 'Composer', $(this).attr('Composer'));
                        listGroupDetail($synthInfo, 'Subtitle', $(this).attr('Subtitle'));
                        listGroupDetail($synthInfo, 'Title', $(this).attr('Title'));
                        listGroupDetail($sheetInfo, 'Arranger', $(this).attr('Arranger'));
                    });
                }
            });
        }
    }
})

const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const pump = require('pump');

//const Fiber = require('fibers');
//const sass = require('gulp-sass');
//sass.compiler = require('sass');

//const sassOptions = {
//    fiber: Fiber,
//    outputStyle: 'compressed',
//    includePaths: [ 'node_modules' ]
//};

function pdf_orbit(cb) {
    pump([
        gulp.src('./node_modules/@tcyr.us/foundation-pdf-orbit/dist/**/*'),
        gulp.dest('./assets/js-vendor')
    ], cb);
}

function vendor_jquery(cb) {
    pump([
        gulp.src([
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/jquery/dist/jquery.min.map'
        ]),
        gulp.dest('./assets/js-vendor')
    ], cb);
}

function vendor_foundation_js(cb) {
    pump([
        gulp.src([
            './node_modules/foundation-sites/dist/js/foundation.min.js',
            './node_modules/foundation-sites/dist/js/foundation.min.js.map'
        ]),
        gulp.dest('./assets/js-vendor')
    ], cb);
}

function vendor_foundation_css(cb) {
    pump([
        gulp.src([
            './node_modules/foundation-sites/dist/css/foundation.min.css',
            './node_modules/foundation-sites/dist/css/foundation.min.css.map'
        ]),
        gulp.dest('./assets/css-vendor')
    ], cb);
}

function vendor_pdfjs(cb) {
    pump([
        gulp.src([
            './node_modules/pdfjs-dist/build/pdf.min.js',
            './node_modules/pdfjs-dist/build/pdf.worker.min.js',
        ]),
        gulp.dest('./assets/js-vendor')
    ], cb);
}

function vendor_what_input(cb) {
    pump([
        gulp.src([
            './node_modules/what-input/dist/what-input.min.js',
            './node_modules/what-input/dist/what-input.min.js.map',
        ]),
        gulp.dest('./assets/js-vendor')
    ], cb);
}

const vendors = gulp.parallel(vendor_jquery, vendor_foundation_js, vendor_foundation_css, vendor_pdfjs, vendor_what_input);

//function assets_scss(cb) {
//    pump([
//        gulp.src('./_assets/scss/**/*.scss'),
//        sourcemaps.init(),
//        sass(sassOptions).on('error', sass.logError),
//        rename({ suffix: '.min' }),
//        sourcemaps.write(''),
//        gulp.dest('./assets/css-compiled')
//    ], cb);
//}

//function assets_js(cb) {
//    pump([
//        gulp.src('./_assets/js/**/*'),
//        gulp.dest('./assets/js')
//    ], cb);
//}

//const assets = gulp.parallel(assets_scss, assets_js);

exports.default = gulp.parallel(pdf_orbit, vendors);
